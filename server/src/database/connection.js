import mongoose from "mongoose";

const uri = "mongodb://localhost/openpaytest";

export const initMongo = async () => {
    try {
        await mongoose.connect(uri, {
            useCreateIndex: true,
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        })
        console.log("Database connected");
    } catch (error) {
        console.log(error);
    }
}