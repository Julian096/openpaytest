import express from "express";
import jwt from "jsonwebtoken";
import Openpay from "openpay";
import bcrypt from "bcrypt";
import { UserSchema } from "../models";

const router = express.Router();
const openpay = new Openpay("m4dd5sjn7g3enokcbvra", "sk_a7a33a9911f54b2b89ec571f129f8419");

router.post("/newUser", async (req, res) => {
    try {
        let userData = req.body;
        const passHashed = await bcrypt.hash(userData.password, 10);
        userData.password = passHashed;
        const newUser = new UserSchema(userData);
        await newUser.save();
        res.send("User created");
    } catch (error) { console.log(error) }
});

router.post("/login", async (req, res) => {
    try {
        const userData = req.body;
        const user = await UserSchema.findOne({ username: userData.username });
        if (user) {
            const isValid = await bcrypt.compare(userData.password, user.password);
            if (isValid) {
                const payload = {
                    id: user.id,
                    name: user.username
                }
                const token = jwt.sign(payload, "openpaytest", { expiresIn: "4h" });
                res.json({ token });
            } else {
                res.status(401);
                res.send("Incorrect user or password");
            }
        } else {
            res.send("User not exist");
        }
    } catch (error) { console.log(error) }
});

router.post("/newClient", (req, res) => {
    try {
        const token = req.headers.authorization.substring(7);
        if (token) {
            jwt.verify(token, "openpaytest", (error) => {
                if (error === null) {
                    let client = req.body;
                    client.requires_account = false;
                    openpay.customers.create(client, (error, customer) => {
                        if (error === null) {
                            res.json(customer);
                        } else {
                            res.status(500);
                            res.json(error);
                        }
                    });
                } else {
                    res.status(401);
                    res.send("Not authorized");
                }
            });
        } else {
            res.status(401);
            res.send("Token not provided");
        }
    } catch (error) {
        res.status(401);
        res.send("Token not provided");
    }
});

router.get("/getAllClients", (req, res) => {
    try {
        const token = req.headers.authorization.substring(7);
        if (token) {
            jwt.verify(token, "openpaytest", (error) => {
                if (error === null) {
                    openpay.customers.list({}, (error, list) => {
                        if (error === null) {
                            if (list.length === 0) {
                                res.send("No hay clientes registrados");
                            } else {
                                res.json(list)
                            }
                        } else {
                            console.log(error);
                            res.status(500);
                            res.send("Error interno");
                        }
                    });
                } else {
                    res.status(401);
                    res.send("Not authorized");
                }
            });
        } else {
            res.status(500);
            res.send("Token not provided");
        }
    } catch (error) {
        res.status(400);
        res.send("Token not provided");
    }
});

router.get("/getClient/:id", (req, res) => {
    try {
        const token = req.headers.authorization.substring(7);
        if (token) {
            jwt.verify(token, "openpaytest", (error) => {
                if (error === null) {
                    const id = req.params.id;
                    openpay.customers.get(id, (error, customer) => {
                        if (error === null) {
                            res.json(customer);
                        } else {
                            console.log(error);
                            res.status(error.http_code);
                            res.json(error);
                        }
                    });
                } else {
                    res.status(401);
                    res.send("Not authorized");
                }
            });
        } else {
            res.status(500);
            res.send("Token not provided");
        }
    } catch (error) {
        res.status(400);
        res.send("Token not provided");
    }
});

router.delete("/deleteClient/:id", (req, res) => {
    try {
        const token = req.headers.authorization.substring(7);
        if (token) {
            jwt.verify(token, "openpaytest", (error) => {
                if (error === null) {
                    const id = req.params.id;
                    openpay.customers.delete(id, (error) => {
                        if (error === null) {
                            res.send("Client deleted");
                        } else {
                            console.log(error);
                            res.status(error.http_code);
                            res.send(error);
                        }
                    });
                } else {
                    res.status(401);
                    res.send("Not authorized");
                }
            });
        } else {
            res.status(500);
            res.send("Token not provided");
        }
    } catch (error) {
        res.status(400);
        res.send("Token not provided");
    }
});

export { router };