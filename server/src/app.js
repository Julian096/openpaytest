import cors from "cors";
import express from 'express';
import { router } from "./routes";
import { initMongo } from "./database/connection";
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use("/", router);

initMongo();
app.listen(3000, () => {
    console.log("Server on port 3000");
});