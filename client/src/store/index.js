import Vue from 'vue'
import Vuex from 'vuex'
import { VueEasyJwt } from "vue-easy-jwt";
import router from "../router";
import axios from 'axios';
import { bus } from "../bus";

const jwt = new VueEasyJwt();

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: {
            id: "",
            name: ""
        }
    },
    mutations: {
        setUsername(state, token) {
            sessionStorage.setItem("token", token);
            const user = jwt.decodeToken(token);
            state.user.id = user.id
            state.user.name = user.name;
            router.push("/dashboard");
        },
        logout() {
            sessionStorage.removeItem("token");
            router.push("/");
        }
    },
    actions: {
        async addClient({ }, payload) {
            try {
                const { data } = await axios.post("/newClient", payload, {
                    headers: {
                        "Authorization": `Bearer ${sessionStorage.getItem("token")}`
                    }
                });
                bus.$emit("clientAdded");
                bus.$emit("clientCreated", data);
            } catch (error) { console.error(error); }
        },
        async getAllClients() {
            try {
                const { data } = await axios.get(`/getAllClients`, {
                    headers: {
                        "Authorization": `Bearer ${sessionStorage.getItem("token")}`
                    }
                });
                bus.$emit("allClients", data);
            } catch (error) { console.log(error) }
        },
        async deleteClient({ }, id) {
            try {
                await axios.delete(`/deleteClient/${id}`, {
                    headers: {
                        "Authorization": `Bearer ${sessionStorage.getItem("token")}`
                    }
                })
            } catch (error) { console.log(error) }
        }
    },
    modules: {
    }
})
